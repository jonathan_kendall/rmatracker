CD directory of program files  *Installation Guide in progress

sudo apt install python3-pip

python3 -m pip install django

apt install libpq-dev python3-dev

sudo apt install postgresql-server-dev-all

sudo apt install postgresql

pip3 install psycopg2

pip3 install fpdf

sudo su - postgres -c "createuser root"

sudo su - postgres -c "createdb mcqs"

sudo -u postgres psql

grant all privileges on database mcqs to root;

ALTER USER root WITH PASSWORD '1234';

python manage.py createsuperuser

workfloor@workfloor:~/question$ python3 manage.py makemigrations
No changes detected

workfloor@workfloor:~/question$ python3 manage.py makemigrations mcqs
Migrations for 'mcqs':
  mcqs/migrations/0001_initial.py
    - Create model choice
    - Create model mcq
    - Create model questions
    - Create model submission
    - Create model myForm

workfloor@workfloor:~/question$ python3 manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, mcqs, sessions
Running migrations:
  Applying mcqs.0001_initial... OK

workfloor@workfloor:~/question$ python3 manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
December 16, 2019 - 01:13:16
Django version 3.0, using settings 'question.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
