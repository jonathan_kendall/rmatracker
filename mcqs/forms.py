from django import forms
from django.contrib.auth import authenticate
from .models import *
from django.contrib.auth.forms import UserCreationForm


class UserLoginForm(forms.Form):
    query=forms.CharField(label="Username",widget=forms.TextInput(attrs={'type':'text','class':'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'type':'password','class':'form-control'}))

    def clean(self,*args,**kwargs):
        username=self.cleaned_data.get("query")
        password=self.cleaned_data.get("password")
        user=authenticate(username=username,password=password)
        if user:
            self.cleaned_data["user_obj"]=user
        else:
            raise forms.ValidationError("Invalid Creditional")
        return super(UserLoginForm,self).clean(*args,**kwargs)


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)


class myFormForm(forms.ModelForm):
    class Meta:
        model=myForm
        fields=('name',)


class questionForm(forms.ModelForm):
    class Meta:
        model=questions
        fields=('question', 'image', 'Attachment')


class mcqForm(forms.ModelForm):
    class Meta:
        model=mcq
        fields=('question',)
