# Generated by Django 3.0.2 on 2020-02-04 06:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mcqs', '0002_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='Attachment',
            field=models.FileField(blank=True, default='', null=True, upload_to='files/'),
        ),
        migrations.AddField(
            model_name='questions',
            name='image',
            field=models.ImageField(blank=True, default='', null=True, upload_to='image/'),
        ),
    ]
