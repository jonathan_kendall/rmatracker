from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class choice(models.Model):
    name = models.CharField(max_length=100, default="test")

    def __str__(self):
        return self.name


class mcq(models.Model):
    question = models.CharField(max_length=500, default="")
    choices = models.ManyToManyField(choice)
    answer = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.question

    def get_cname(self):
        return 'mcq'


class questions(models.Model):
    question = models.CharField(max_length=500, default="")
    image = models.ImageField(upload_to='image/', blank=True, null=True, default="")
    Attachment = models.FileField(upload_to='files/', blank=True,  null=True, default="")
    answer = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.question

    def get_cname(self):
        return 'question'


class myForm(models.Model):
    name = models.CharField(max_length=100, default="Question Survey")
    mcqs = models.ManyToManyField(mcq)
    questions = models.ManyToManyField(questions)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('detailform', kwargs={"pk": self.pk})

    def get_update_url(self):
        return reverse('updateform', kwargs={"pk": self.pk})

    def get_delete_url(self):
        return reverse('deleteform', kwargs={"pk": self.pk})


class submission(models.Model):
    submittedby = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, default="Question Survey")
    mcqs = models.ManyToManyField(mcq)
    questions = models.ManyToManyField(questions)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('detailsubmission', kwargs={"pk": self.pk})

    def get_delete_url(self):
        return reverse('deletesubmission', kwargs={"pk": self.pk})


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150)
    #signup_confirmation = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User, dispatch_uid='save_new_user_profile')
def save_profile(sender, instance, created, **kwargs):
    user = instance
    if created:
        profile = Profile(user=user)
        profile.save()
