from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, FileResponse

from .forms import UserLoginForm, myFormForm, questionForm, mcqForm, SignUpForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, get_user_model, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from .models import *
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views import View
import json
from django.middleware import csrf
from django.template.loader import get_template
from fpdf import FPDF, HTMLMixin
import csv


# Create your views here.
def index(request):
    if request.user.is_authenticated:
        forms = myForm.objects.all()
        context = {'forms': forms}
        return render(request, 'home.html', context)
    else:
        return redirect('/login')


class renderform(View):
    def get(self, *args, **kwargs):
        context = {"object": myForm.objects.get(pk=self.kwargs['pk']), 'csrf_token': csrf.get_token(self.request)}
        return render(self.request, 'form.html', context)

    def post(self, *args, **kwargs):
        error = False
        try:
            obj = myForm.objects.get(pk=self.kwargs['pk'])
            newsubmission = submission(name=obj.name, submittedby=self.request.user)
            newsubmission.save()
            for q in obj.questions.all():
                answer = self.request.POST['question-' + str(q.pk)]
                newq = questions(question=q.question, answer=answer)
                newq.save()
                newsubmission.questions.add(newq)
            for m in obj.mcqs.all():
                answer = self.request.POST['mcq-' + str(m.pk)]
                newq = mcq(question=m.question, answer=answer)
                newq.save()
                newsubmission.mcqs.add(newq)
        except:
            error = True
        context = {"error": error}
        return render(self.request, 'submission.html', context)


def user_login(request, *args, **kwargs):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get("user_obj")
        c = login(request, user_obj)
        return HttpResponseRedirect('/')
    else:
        pass
    return render(request, "login.html", {"form": form})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            print(form.cleaned_data.get('email'))
            user.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


class SubmissionList(ListView):
    model = submission
    template_name = 'Submissions/list.html'


class SubmissionDetail(DetailView):
    model = submission
    template_name = 'Submissions/detail.html'


class SubmissionDelete(DeleteView):
    model = submission
    template_name = 'Submissions/delete.html'
    success_url = '/submissions'


class FormList(ListView):
    model = myForm
    template_name = 'Forms/list.html'


class FormCreate(CreateView):
    form_class = myFormForm
    template_name = 'Forms/create.html'
    success_url = '/forms'


class FormUpdate(UpdateView):
    model = myForm
    form_class = myFormForm
    template_name = 'Forms/update.html'
    success_url = '/forms'


class FormDetail(DetailView):
    model = myForm
    template_name = 'Forms/detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(FormDetail, self).get_context_data(*args, **kwargs)
        context['pk'] = self.kwargs['pk']
        return context


class FormDelete(DeleteView):
    model = myForm
    template_name = 'Forms/delete.html'
    success_url = '/forms'


class QuestionCreate(CreateView):
    form = questionForm
    template_name = 'Questions/create.html'
    fields = ('question', 'image', 'Attachment')

    def get_queryset(self):
        new_context = questions.objects.all()
        return new_context

    def form_valid(self, form):
        form.save()
        myform = myForm.objects.get(pk=self.kwargs['pk'])
        myform.questions.add(form.instance)
        return HttpResponseRedirect(reverse('detailform', kwargs={"pk": self.kwargs['pk']}))


class QuestionDelete(DeleteView):
    model = questions
    template_name = 'Questions/delete.html'
    success_url = '/forms'

    def delete(self, request, *args, **kwargs):
        x = super(QuestionDelete, self).delete(request, *args, **kwargs)
        return HttpResponseRedirect(reverse('detailform', kwargs={"pk": self.kwargs['pk2']}))


class QuestionUpdate(UpdateView):
    model = questions
    form_class = questionForm
    template_name = 'Questions/update.html'
    success_url = '/forms'

    def get_success_url(self):
        return reverse('detailform', kwargs={"pk": self.kwargs['pk2']})


class McqCreate(CreateView):
    form = mcqForm
    template_name = 'MCQS/create.html'
    fields = ('question',)

    def get_queryset(self):
        new_context = mcq.objects.all()
        return new_context

    def form_valid(self, form):
        form.save()
        myform = myForm.objects.get(pk=self.kwargs['pk'])
        myform.mcqs.add(form.instance)
        return HttpResponseRedirect(reverse('detailform', kwargs={"pk": self.kwargs['pk']}))


class McqDelete(DeleteView):
    model = mcq
    template_name = 'MCQS/delete.html'
    success_url = '/forms'

    def get_success_url(self):
        return reverse('detailform', kwargs={"pk": self.kwargs['pk2']})


class McqUpdate(UpdateView):
    model = mcq
    form_class = mcqForm
    template_name = 'MCQS/update.html'
    success_url = '/forms'

    def get_success_url(self):
        return reverse('detailform', kwargs={"pk": self.kwargs['pk2']})


class addchoice(View):
    def post(self, *args, **kwargs):
        data = json.loads(self.request.body.decode('utf8').replace("'", '"'))
        try:
            newchoice = choice(name=data['value'])
            newchoice.save()
            currentmcq = mcq.objects.get(pk=data['id'])
            currentmcq.choices.add(newchoice)
            return HttpResponse('ok')
        except:
            return HttpResponse('failed')


def pdfMaker(request, id):
    class MyFPDF(FPDF, HTMLMixin):
        pass

    pdf = MyFPDF()
    pdf.add_page()
    pdf.set_font("Arial", size=12)

    Submissions = submission.objects.filter(pk=id)

    for sub in Submissions:
        pdf.cell(200, 10, txt=f"{sub.name}", ln=1, align="C")
        pdf.cell(200, 10, txt="", ln=1, align="C")

        pdf.cell(200, 10, txt=f"QUESTIONS", ln=1, align="C")
        pdf.cell(200, 10, txt="", ln=1, align="C")

        for idx, ques in enumerate(sub.questions.all()):
            pdf.cell(200, 6, txt=f"{idx+1} - {ques}", ln=1, align="L")
            pdf.cell(200, 6, txt=f"Answer. {ques.answer}", ln=1, align="L")
            pdf.cell(200, 10, txt=" ", ln=1, align="L")

        pdf.cell(200, 10, txt=f"MCQS", ln=1, align="C")
        pdf.cell(200, 10, txt="", ln=1, align="C")
        
        for idx, mcq in enumerate(sub.mcqs.all()):
            pdf.cell(200, 6, txt=f"{idx+1} - {mcq}", ln=1, align="L")
            pdf.cell(200, 6, txt=f"Answer: {mcq.answer}", ln=1, align="L")
            pdf.cell(200, 10, txt=" ", ln=1, align="L")    

    pdf.output("submission.pdf")

    with open("submission.pdf", "rb") as f:
        response = HttpResponse(f.read(), content_type="application/pdf")
        response['Content-Disposition'] = 'inline; filename=submission.pdf'
        return response


def csvMaker(request, id):
    Submissions = submission.objects.filter(pk=id)
    with open('submission.csv', 'w', newline='') as file:
        writer = csv.writer(file)

        for sub in Submissions:
            for ques in sub.questions.all():
                writer.writerow([ques, ques.answer])
            for mcq in sub.mcqs.all():
                writer.writerow([mcq, mcq.answer])
        file.close()

    with open("submission.csv", "rb") as f:
        response = HttpResponse(f.read(), content_type="application/vnd.ms-excel")
        response['Content-Disposition'] = 'inline; filename=submission.csv'
        return response
