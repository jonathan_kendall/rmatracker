"""
WSGI config for question project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys

root = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, root)

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'question.settings')

application = get_wsgi_application()

"""
exposes the WSGI callable as a module-level variable named ``application``. 
For more information on this file, see 
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/ 
""" 
# import os 
# import time 
# import traceback 
# import signal 
# import sys 
 
# from django.core.wsgi import get_wsgi_application 
 
# sys.path.append('/var/www/html/rmatracker') 
# # adjust the Python version in the line below as needed 
# sys.path.append('/var/www/html/rmatracker/venv/lib/python3.6/site-packages') 
 
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "question.settings")
 
# try: 
    # application = get_wsgi_application() 
# except Exception: 
    # # Error loading applications 
    # if 'mod_wsgi' in sys.modules: 
        # traceback.print_exc() 
        # os.kill(os.getpid(), signal.SIGINT) 
        # time.sleep(2.5) 